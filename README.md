![](http://digitalknob.com/Digitalknob/Digitalknob/logo.png)
# DigitalKnob

<!-- [![Gitter](https://badges.gitter.im/DigitalKnob/community.svg)](https://gitter.im/DigitalKnob/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge) -->

### Digitalknob is a Cross-Platform Solution that embraces C++, JavaScript, Html, and CSS.
*"Apps should look and act the same on every operating system and browser."* <br><br>

### Supported Platforms
Windows 32 <br>
Windows 64 <br>
Mac 64 <br>
Linux 32 <br>
Linux 64 <br>
iPhone/iPad 64 (<br>
iPhone/iPad-Simulator 64 <br>
Android 32 <br>
Android 64 <br>
Raspberry Pi <br>
Chrome <br>
Safari <br>
Firefox <br>
Opera <br>
Internet Explorer (9.0+) <br>
Emscripten <br>

### Supported Frameworks
[SDL](https://github.com/spurious/SDL-mirror) <br>
<!-- [SFML](https://github.com/SFML/SFML) -->
[OpenSceneGraph](https://github.com/openscenegraph/osg) <br>
<!-- [OpenFrameworks](https://github.com/openframeworks/openFrameworks) -->
[Emscripten](https://github.com/kripken/emscripten) <br>

### Supported Tools, Libraries and Frameworks
[agl](https://developer.apple.com/documentation/agl/agl?language=objc) <br>
[android-ndk](https://github.com/android/ndk) <br>
[ant](https://github.com/apache/ant) <br>
<!-- [aom](https://aomedia.googlesource.com/aom) <br> -->
[appkit](https://developer.apple.com/documentation/appkit?language=objc) <br>
[astyle](http://astyle.sourceforge.net/) <br>
<!-- [aubio](https://github.com/aubio/aubio) <br> -->
[audio_toolbox](https://developer.apple.com/documentation/audiotoolbox?language=objc) <br>
[audiounit](https://developer.apple.com/documentation/audiotoolbox/audiounit?language=objc) <br>
[autotools](https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html) <br>
[avfoundation](https://developer.apple.com/documentation/avfoundation?language=objc) <br>
[avkit](https://developer.apple.com/documentation/avkit?language=objc) <br>
[avf_audio](https://developer.apple.com/documentation/avfaudio?language=objc) <br>
[babel](https://github.com/babel/babel) <br>
[boost](https://github.com/boostorg/boost) <br>
[boxer](https://github.com/aaronmjacobs/Boxer) <br>
[build-essential](https://packages.ubuntu.com/focal/build-essential) <br>
[bullet3](https://github.com/bulletphysics/bullet3) <br>
[bzip2](https://github.com/asimonov-im/bzip2) <br>
[carbon](https://developer.apple.com/documentation/coreservices/carbon_core) <br>
[cef_binary](https://bitbucket.org/chromiumembedded/cef) <br>
[chart.js](https://github.com/chartjs/Chart.js) <br>
[cmake](https://cmake.org) <br>
[cocoa](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/CocoaFundamentals/WhatIsCocoa/WhatIsCocoa.html) <br>
[codemirror](https://github.com/codemirror/CodeMirror) <br>
<!-- [command_line_tools]() <br> -->
<!-- [conio-for-linux](https://github.com/nowres/conio-for-linux) <br> -->
<!-- [content](https://github.com/mdn/content) <br> -->
[core_audio](https://developer.apple.com/library/archive/documentation/MusicAudio/Conceptual/CoreAudioOverview/WhatisCoreAudio/WhatisCoreAudio.html) <br>
[core_foundation](https://developer.apple.com/documentation/corefoundation) <br>
[core_graphics](https://developer.apple.com/documentation/coregraphics?language=objc) <br>
[core_haptics](https://developer.apple.com/documentation/corehaptics?language=objc) <br>
[core_motion](https://developer.apple.com/documentation/coremotion?language=objc) <br>
[core_services](https://developer.apple.com/documentation/coregraphics?language=objc) <br>
[core_video](https://developer.apple.com/documentation/corevideo) <br>
<!-- [cpp-subprocess](https://github.com/tsaarni/cpp-subprocess) <br> -->
<!-- [crosswalk]() <br> -->
[cryptopp](https://github.com/weidai11/cryptopp) <br>
[curl](https://github.com/bagder/curl) <br>
[duktape](https://github.com/svaarala/duktape) <br>
<!-- [ffmpeg](https://github.com/FFmpeg/FFmpeg) <br> -->
<!-- [freealut](https://github.com/vancegroup/freealut) <br> -->
[freetype](https://github.com/vinniefalco/FreeType) <br>
[giflib](http://sourceforge.net/projects/giflib/files/) <br>
[imagemagick](https://github.com/ImageMagick/ImageMagick/tree/master/Magick%2B%2B) <br>
<!-- [libjpeg](https://github.com/LuaDist/libjpeg) <br> -->
[libjpeg-turbo](https://github.com/libjpeg-turbo/libjpeg-turbo) <br>
[libarchive](https://github.com/libarchive/libarchive) <br>
[libiconv](http://gnuwin32.sourceforge.net/packages/libiconv.htm) <br>
<!-- [libtorrent](https://github.com/arvidn/libtorrent) <br> -->
[libuv](https://github.com/libuv/libuv) <br>
<!-- [libvncserver](https://github.com/LibVNC/libvncserver) <br> -->
[libwebsockets](https://github.com/warmcat/libwebsockets) <br>
[libxml2](http://xmlsoft.org/) <br>
[leptonica](https://github.com/DanBloomberg/leptonica) <br>
[msinttypes](https://github.com/chemeris/msinttypes)<br>
[ogg](https://github.com/gcp/libogg) <br>
[openal](https://github.com/apportable/openal-soft) <br>
[opencv](https://github.com/opencv/opencv) <br>
<!-- [openframeworks](https://github.com/openframeworks/openFrameworks) <br> -->
[openscenegraph](https://github.com/openscenegraph/osg) <br>
[openssl](http://www.npcglib.org/~stathis/blog/precompiled-openssl/) <br>
<!-- [osgaudio](https://github.com/mccdo/osgaudio) <br> -->
<!-- [osgbullet](https://github.com/mccdo/osgbullet) <br> -->
[osgworks](https://github.com/mccdo/osgworks) <br>
[libpng](https://github.com/coapp-packages/libpng) <br>
[pofodo](http://podofo.sourceforge.net) <br>
[pugixml](https://github.com/zeux/pugixml) <br>
[rmlui](https://github.com/mikke89/RmlUi) <br>
<!-- [rtaudio](https://github.com/thestk/rtaudio) <br> -->
[rtmidi](https://github.com/thestk/rtmidi) <br>
[sdl](https://github.com/libsdl-org/SDL) <br>
[sdl2_gif](http://themealena.fr/html/SDL_GIFlib.php) <br>
[sdl_image](https://www.libsdl.org/projects/SDL_image/) <br>
[sdl_mixer](https://github.com/aduros/SDL_mixer) <br>
[sdl_ttf](https://www.libsdl.org/projects/SDL_ttf/) <br>
[sfml](https://github.com/SFML/SFML) <br>
[smpeg2](https://github.com/ErintLabs/SDL_mixer/tree/master/external/smpeg2-2.0.0) <br>
[stackwalker](https://stackwalker.codeplex.com/) <br>
<!-- [tesseract](https://github.com/tesseract-ocr/tesseract) <br> -->
[threadpool](http://threadpool.sourceforge.net/) <br>
[tidy-html5](http://tidy.sourceforge.net/) <br>
[tiff](https://github.com/LuaDist/libtiff) <br>
[uwebsockets](https://github.com/uNetworking/uWebSockets) <br>
[vorbis](https://github.com/soundcloud/vorbis) <br>
<!-- [waave](https://github.com/grepwood/waave) <br> -->
[webview](https://developer.android.com/reference/android/webkit/WebView.html) <br>
[xz](https://github.com/nobled/xz) <br>
[zlib](https://github.com/madler/zlib) <br>


# How to build
### Windows Host
Download and run [build.bat](https://github.com/aquawicket/DigitalKnob/releases/download/1.0b/build.cmd) 

### Unix Host
Download [build.sh](https://github.com/aquawicket/DigitalKnob/releases/download/1.0b/build.sh) <br>
From terminal run 'chmod 777 /path/to/build.sh' <br>
Run '/path/to/build.sh'


<br><br><br>
[Forking, Branching and Pull Requests](https://github.com/Kunena/Kunena-Forum/wiki/Create-a-new-branch-with-git-and-manage-branches)<br>
[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
[.](http://aquawicket.github.io/DigitalKnob/DKPlugins/index.html)


# License
DigitalKnob is published under the MIT license. 
This repository references the use of third-party source code and assets with their own licenses. Upon any use of DigitalKnob and/or any part of its code base, you hereby fully agree to, and acknowledge all licensees of said third-party source code.

MIT License

Copyright (c) 2010-2023 DigitalKnob, and contributors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
