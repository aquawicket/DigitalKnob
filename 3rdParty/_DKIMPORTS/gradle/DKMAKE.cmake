# https://github.com/gradle/gradle

dk_undepend(gradle)
dk_return() # FIXME: currently disabled

dk_depend(openjdk)

dk_import(https://github.com/gradle/gradle.git)
