# https://github.com/dotnet/ILMerge
# https://www.nuget.org/packages/ilmerge



dk_import(https://github.com/dotnet/ILMerge.git)

#dk_import(https://github.com/dotnet/ILMerge/archive/9ec1b3f29f6f11b8fc2ddcca4c055dae7b10d9e3.zip)

#dk_set(ILMERGE_VERSION 3.0.41)
#dk_set(ILMERGE_FOLDER ilmerge-${ILMERGE_VERSION})
#dk_set(ILMERGE_DL https://www.nuget.org/api/v2/package/ilmerge/3.0.41)
#dk_set(ILMERGE ${3RDPARTY}/${ILMERGE_FOLDER})
#dk_import(${ILMERGE_DL} ${ILMERGE})
